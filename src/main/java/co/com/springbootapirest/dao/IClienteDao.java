package co.com.springbootapirest.dao;



import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IClienteDao extends JpaRepository<Cliente, Long> {


}
